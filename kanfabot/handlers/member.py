import logging
from typing import Optional

import peewee
from telegram import User, Chat
from telegram.constants import ChatType

from kanfabot.models.models import ConfMember, Conf

logger = logging.getLogger(__name__)


# Add group member to the database if the record doesn't exist
def get_or_create_conf_member(user: User, effective_chat: Chat) -> Optional[ConfMember]:
    if effective_chat.type in [ChatType.GROUP, ChatType.SUPERGROUP]:
        conf: Conf
        created: bool
        conf, created = Conf.get_or_create(tg_id=effective_chat.id)
        if created or conf.type != effective_chat.type:
            logger.info(f"Successfully added conf {effective_chat.title} to the database")
            conf.type = effective_chat.type
            conf.save()

        member: ConfMember
        member_created: bool
        try:
            member, member_created = ConfMember.get_or_create(tg_id=user.id, conf=conf)
        except peewee.IntegrityError as e:
            logger.error(f"can't add new user {user.full_name} from {effective_chat.full_name} to the database: {e}")
            return None

        if user.full_name != member.name or member_created:
            member.name = user.full_name
        if not member.is_active:
            logger.info(f"Set chat member {member.name} activity status to true")
            member.is_active = True
        member.save()

        if member_created:
            logger.info(f"Successfully added user {member.name} from {effective_chat.title} to the database")

        return member
    return None
