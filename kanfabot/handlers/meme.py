import inspect
import logging
import os
import sys
import uuid

from typing import Optional, TypeAlias, Union

import peewee
import telegram

from telegram import Bot, Update, Document, PhotoSize, Audio, Voice, Video
from telegram.ext import ContextTypes

from kanfabot.models.models import Meme, Conf, ConfMember
from kanfabot.utils import utils, paths

logger = logging.getLogger(__name__)

SupportedAttachmentTypes: TypeAlias = Union[Document, tuple[PhotoSize], Audio, Voice, Video]


def init_memes_folder():
    if not os.path.exists(paths.MEMES_PATH):
        try:
            os.mkdir(paths.MEMES_PATH)
            os.mkdir(paths.STATIC_MEDIA_PATH)
        except PermissionError:
            logger.error("Couldn't create memes folder, permission denied")
            sys.exit(1)

    if not os.path.exists(os.path.join(paths.STATIC_MEDIA_PATH, 'coke1')):
        logger.warning("Couldn't find coke1 pic inside static folder")
    if not os.path.exists(os.path.join(paths.STATIC_MEDIA_PATH, 'coke2')):
        logger.warning("Couldn't find coke1 pic inside static folder")

    logger.info(f"Successfully initialized memes folder at {os.path.abspath(paths.MEMES_PATH)}")


async def _send_meme(meme_db_record: Meme, chat_id: int, bot: Bot) -> None:
    with open(os.path.join(paths.MEMES_PATH, str(meme_db_record.uuid)), "rb") as file:
        try:
            match meme_db_record.type:
                case Meme.DOCUMENT:
                    await bot.send_document(chat_id=chat_id, document=file, filename=meme_db_record.title)
                case Meme.PHOTO:
                    await bot.send_photo(chat_id=chat_id, photo=file)
                case Meme.AUDIO:
                    await bot.send_audio(chat_id=chat_id, audio=file, title=meme_db_record.title)
                case Meme.VOICE:
                    await bot.send_voice(chat_id=chat_id, voice=file)
                case Meme.VIDEO:
                    await bot.send_video(chat_id=chat_id, video=file)
                case _:
                    await bot.send_message(chat_id=chat_id, text="бд сус")
                    logger.warning(f"Database error. Could not match meme type {meme_db_record.type} in random_meme")
                    return
        except telegram.error.NetworkError as e:
            await bot.send_message(chat_id=chat_id, text="тг сус")
            logger.warning(f"Telegram network error. Could not send {meme_db_record} meme in random_meme function, {e}")
            return


# this handler only takes updates that have a number after the prefix and command name
# TODO: fix memes from any conf can be retrieved
async def send_meme_by_index(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await utils.process_message(update, context)

    if utils.is_edit_or_forward(update):
        return

    index_str = update.effective_message.text.split()[-1]
    index = int(index_str)
    if index_str[0] == "-":
        memes_number: int = (Meme.
                             select().
                             count()
                             )
        # ids actually start from 1. who would've thought?
        index = memes_number - abs(index) + 1

    meme_record: Optional[Meme] = Meme.get_or_none(Meme.id == index)
    if meme_record is None:
        logger.warning(f"Could not find a meme inside the db with id {index}")
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="мем с таким индексом не был найден")
        return

    await _send_meme(meme_db_record=meme_record, chat_id=update.effective_chat.id, bot=context.bot)


# Sends a random picture from the memes folder
async def send_random_meme(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await utils.process_message(update, context)

    if utils.is_edit_or_forward(update):
        return

    this_conf: Conf = Conf.get_or_none(tg_id=update.effective_chat.id)
    query: peewee.ModelSelect = (Meme.
                                 select().
                                 where((Meme.belongs_to == this_conf) |
                                       (Meme.belongs_to == None)
                                       ).
                                 order_by(peewee.fn.Random()).
                                 limit(1)
                                 )
    try:
        meme_record: Meme = query[0]
    except IndexError:
        await context.bot.send_message(chat_id=update.effective_chat.id, text="мемов нет.")
        return

    await _send_meme(meme_db_record=meme_record, chat_id=update.effective_chat.id, bot=context.bot)


def _get_meme_type_str(attachment) -> Optional[str]:
    meme_types = {
        telegram.PhotoSize: "photo",
        telegram.VideoNote: "video",
        telegram.Voice: "voice",
        telegram.Video: "video",
        telegram.Animation: "video",
        telegram.Audio: "audio",
        telegram.Document: "document",
    }
    return meme_types.get(type(attachment))


def _get_meme_type_obj(string: str) -> Optional[object]:
    meme_types = {
        "photo": telegram.PhotoSize,
        "video_note": telegram.Video,
        "animation": telegram.Video,
        "voice": telegram.Voice,
        "video": telegram.Video,
        "audio": telegram.Audio,
        "document": telegram.Document,
    }
    return meme_types.get(string)


# Adds the provided meme to the memes folder
# All the updates handled by this function are either:
# 1) reply to an attachment
# 2) caption to an attachment
async def add_meme(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await utils.process_message(update, context)

    if utils.is_edit_or_forward(update):
        return

    if update.effective_message.reply_to_message is None and update.effective_message.effective_attachment is None:
        await update.effective_message.reply_text(
            text="To add a meme you need to either reply to it or send it with a caption"
        )
        logger.info(
            f"User {update.effective_user.full_name} tried to add a meme without replying to it or sending it"
        )
        return

    if update.effective_chat.type == telegram.constants.ChatType.PRIVATE:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Can't add memes outside of groups!"
        )
        logger.info(f"User {update.effective_user.full_name} tried to add a meme in private chat")

    # TODO: Since Telegram views every attachment as a separate update, adding multiple attachments at once would
    #   require to know the amount of updates to process. This could be implemented with an optional command argument.
    #   example: "соня мем 3" will process 3 updates, the caption with the first attachment, and the two updates after.
    #   a drawback of such approach is the fact that sometimes the command is not a caption to the first attachment,
    #   but rather is a standalone message (like when sending 2 or more files)

    attachment: SupportedAttachmentTypes = (
            update.effective_message.effective_attachment or
            update.effective_message.reply_to_message.effective_attachment
    )

    # if the attachment is a tuple of PhotoSizes, overwrite it with the highest resolution photo
    if isinstance(attachment, tuple) and isinstance(attachment[-1], telegram.PhotoSize):
        attachment = attachment[-1]

    try:
        new_meme_file = await attachment.get_file()
    except telegram.error.BadRequest:
        await context.bot.send_message(chat_id=update.effective_chat.id, text="Слишком большой файл!")
        logger.warning(f"{update.effective_user.full_name}, id:{update.effective_user.id} "
                       f"tried to add a meme that has too big file size")
        return

    new_meme_name = str(uuid.uuid4())
    if isinstance(attachment, (telegram.Audio, telegram.Document)):
        new_meme_title = attachment.file_name
    else:
        new_meme_title = None
    new_meme_type: str = _get_meme_type_str(attachment)

    new_meme_creator: Optional[ConfMember] = ConfMember.get_or_none(tg_id=update.effective_user.id)
    assert new_meme_creator is not None

    new_meme_conf: Optional[Conf] = Conf.get_or_none(tg_id=update.effective_chat.id)
    assert new_meme_conf is not None

    try:
        new_meme: Meme = Meme.create(
            uuid=new_meme_name,
            title=new_meme_title,
            type=new_meme_type,
            # mime type gets updated later on, after the attachment is downloaded
            # mime_type=,
            added_by=new_meme_creator,
            # belongs_to=new_meme_conf
        )
    except peewee.IntegrityError:
        await context.bot.send_message(chat_id=update.effective_chat.id, text="Ошибка бд!")
        logger.warning(f"{update.effective_user.full_name}, id:{update.effective_user.id} "
                       f"tried to add a file: {new_meme_name},\n"
                       f"{new_meme_title},\n"
                       f"{new_meme_type},\n"
                       f"{new_meme_creator},\n"
                       f"{new_meme_conf}\n that caused database integrity error")
        return
    new_meme.save()

    await new_meme_file.download_to_drive(os.path.join(paths.MEMES_PATH, new_meme_name))
    # memes.append(new_meme_name)

    new_meme_mime_type = utils.get_mime_type(os.path.join(paths.MEMES_PATH, new_meme_name))
    new_meme.mime_type = new_meme_mime_type
    new_meme.save()

    new_meme_conf_chat = await context.bot.get_chat(chat_id=new_meme_conf.tg_id)
    logger.info(
        f"{new_meme_creator.name} at {new_meme_conf_chat.effective_name},"
        f" {new_meme_conf.tg_id} conf created new {new_meme_type} meme"
        f" with {new_meme_mime_type} mime type")

    await context.bot.send_message(chat_id=update.effective_chat.id, text="мем добавлен")
