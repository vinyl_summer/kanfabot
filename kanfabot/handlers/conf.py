# ruff: noqa: E712,

import inspect
import logging
import random
import re
from typing import List, Optional

import peewee
import telegram.error

from telegram import Update, ChatMember, User, Message
from telegram.ext import ContextTypes

from kanfabot.utils import utils, paths

from kanfabot.models.models import ConfMember, Conf
from kanfabot.utils.utils import process_message, is_edit_or_forward, check_if_admin
from kanfabot.app import app

logger = logging.getLogger(__name__)


async def _timer_callback(context: ContextTypes.DEFAULT_TYPE) -> None:
    job = context.job
    message_id: int = job.data

    percentage_chance = 0.01
    if random.random() < percentage_chance:
        picture = 'coke2'
    else:
        picture = 'coke1'

    picture_path = paths.STATIC_MEDIA_PATH + picture
    try:
        with open(picture_path, "rb") as file:
            await context.bot.send_photo(
                chat_id=job.chat_id,
                photo=file,
                reply_to_message_id=message_id
            )
    except FileNotFoundError as e:
        logger.error(f"Couldn't open file {picture}, {e}")
        await context.bot.send_message(
            chat_id=job.chat_id,
            reply_to_message_id=message_id,
            text="кола готова"
        )


async def create_timer_job(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await process_message(update, context)

    if is_edit_or_forward(update):
        return

    chat_id: int = update.effective_message.chat_id
    message_id: int = update.effective_message.id

    responses = {
        "Aryan": "Treffen Sie mich in {} Minuten",
        "default": "Встретимся через {} минут",
        "reject": "Много хочешь",
    }

    should_reject = False

    try:
        due = float(update.message.text[7:].strip())

        match due:
            case 1488:
                response: str = responses['Aryan'].format(int(due))

            case _ if (due < 0 or due > 60 * 24):
                response: str = responses['reject']
                should_reject = True

            case _:
                response: str = responses['default'].format(int(due))

        await update.effective_message.reply_text(response)

        if should_reject:
            return

        context.job_queue.run_once(_timer_callback, due * 60, chat_id=chat_id, data=message_id)
        logger.info(f"Created timer for {update.effective_user.full_name} due {due} minutes")
    except (IndexError, ValueError):
        await update.effective_message.reply_text("Usage: а кола <минуты>")


async def _apply_chance_reactions(target_message: Message, user_message: str, random_chance: int) -> None:
    if random_chance == 101:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.FIRE)

    elif random_chance == 100:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.HUNDRED_POINTS_SYMBOL)

    elif random_chance == 0:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.THUMBS_DOWN)

    elif random_chance == 69:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.THUMBS_UP)

    elif user_message == '4' and random_chance == 20:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.FACE_WITH_UNEVEN_EYES_AND_WAVY_MOUTH)

    elif user_message == '9' and random_chance == 11:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.DOVE_OF_PEACE)

    elif user_message == '13' and random_chance == 37:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.SMILING_FACE_WITH_SUNGLASSES)

    elif user_message == '14' and random_chance == 88:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.SALUTING_FACE)

    # not rolling, got a chance that would score
    elif user_message not in {'72', '7'} and random_chance in {7, 27}:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.ROLLING_ON_THE_FLOOR_LAUGHING)


# Sends the message that the user sent with a random chance at the end
async def send_random_chance(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await process_message(update, context)

    if is_edit_or_forward(update):
        return

    random_chance: int = utils.get_random_chance()
    user_message: str = ' '.join(update.effective_message.text.split()[2:])

    await _apply_chance_reactions(
        update.effective_message,
        user_message,
        random_chance
    )

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Шанс того, что {user_message} равен {random_chance}%"
    )


# Sends the user back one of the provided options
async def send_random_choice(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await process_message(update, context)

    if is_edit_or_forward(update):
        return

    text = " ".join(update.message.text.split()[2:])
    delimiters = [',',
                  ' или ',
                  ' or ',
                  ' ',
                  ]
    choices = []

    # delimiters are evaluated from the most to the least explicit
    for delimiter in delimiters:
        if delimiter in text:
            choices: List[str] = text.split(delimiter)
            choices = [x.strip() for x in choices]
            break

    random_choice: str = random.choice(choices)

    response = random.choice(
        [
            f"Я советую тебе {random_choice}",
            f"Думаю, стоит выбрать {random_choice}",
            f"Несомненно {random_choice}",
            f"Если придётся выбирать, то уж лучше {random_choice}",
            f"Выбирай {random_choice} или лох",
        ]
    )

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=response
    )


# Sends a random users name from the group
async def send_random_participant_name(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")
    await process_message(update, context)

    if is_edit_or_forward(update):
        return
    # IMPORTANT!!! do NOT "fix" .is_active == True, ORM will break
    this_conf: Conf = Conf.get(tg_id=update.effective_chat.id)
    conf_members: peewee.ModelSelect
    conf_members = (ConfMember.
                    select().
                    where(
                        (ConfMember.conf == this_conf) &
                        (ConfMember.is_active == True)
                        )
                    )

    member: ConfMember  # noqa: F842
    tg_member_ids: List[int] = [member.tg_id for member in conf_members]

    if len(tg_member_ids) == 0:
        logger.warning(f"Couldn't find any users in conf {update.effective_chat.id}")
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Произошла ошибка, проверьте список айдишников"
        )
        return

    chat_id: int = update.message.chat_id

    random_member: ChatMember | None = None
    for _ in range(len(tg_member_ids)):
        random_member_id: int = random.choice(tg_member_ids)
        try:
            random_member: ChatMember = await context.bot.get_chat_member(chat_id, random_member_id)
        except telegram.error.BadRequest as e:
            if e.message == "User not found":
                tg_member_ids.remove(random_member_id)

                inactive_member: ConfMember = ConfMember.get(
                    tg_id=random_member_id,
                    conf=this_conf
                )
                inactive_member.is_active = False
                inactive_member.save()

                logger.warning(
                    f"Couldn't find member {inactive_member} in conf {update.effective_chat.id}, "
                    "marking them as inactive"
                )
                await context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    text=f"Marked {inactive_member.name} as inactive"
                )

    user_name: str = random_member.user.full_name
    text: str = update.effective_message.text
    user_message_start: int = 0
    for case in app.WHO_CASES[1:-1].split("|"):
        case_match = re.search(fr'\b({re.escape(case)})\b', text)
        if not case_match:
            continue

        user_message_start = case_match.start() + len(case)
        break

    user_text = text[user_message_start:].strip()

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Несомненно, {user_name} {user_text}"
    )


# Adds new group member to the database and greets them
async def add_conf_member(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await process_message(update, context)

    new_member: User = update.effective_message.new_chat_members[0]

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"ну здарова {new_member.full_name}"
    )


# Removes left group member from the database
async def remove_conf_member(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    left_user: User = update.effective_message.left_chat_member

    chat_id: int = update.effective_chat.id
    user_tg_id: int = left_user.id
    user_name: str = left_user.full_name

    await context.bot.send_message(chat_id=chat_id, text=f"прощяй {user_name}")

    left_chat_member: ConfMember = ConfMember.get_or_none(tg_id=user_tg_id)
    if left_chat_member is None:
        logger.warning(f"{user_name}, {user_tg_id} left but had no record in the database")
        return
    left_chat_member.is_active = False
    left_chat_member.save()

    logger.info(f"Set chat member {user_name} activity status to false")


@check_if_admin
async def show_members(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await process_message(update, context)

    if is_edit_or_forward(update):
        return

    response = ''
    try:
        confs: peewee.ModelSelect = Conf.select()
    except peewee.PeeweeException as e:
        logger.exception("Couldn't select all confs from the database", exc_info=e)
        await update.effective_message.reply_text("Database error")
        return

    # remove Nones if present
    confs: List[Conf] = [conf for conf in list(confs) if conf is not None]

    for conf in confs:
        try:
            chat = await context.bot.get_chat(chat_id=conf.tg_id)
        except telegram.error.TelegramError as e:
            logger.exception(f"Couldn't get chat {conf.tg_id} info from telegram", exc_info=e)
            await update.effective_message.reply_text("Telegram error")
            return
        response += f"Conf : {chat.effective_name}\n"

        members: peewee.ModelSelect = ConfMember.select().where(ConfMember.conf == conf)
        members: List[ConfMember] = [member for member in list(members) if member is not None]

        for i, member in enumerate(members):
            if not member.is_active:
                response += f"\t\t{i + 1}) {member.name} RIP\n"
            else:
                response += f"\t\t{i + 1}) {member.name}\n"

    if response == '':
        response = 'No members found'

    await update.message.reply_text(text=response)


def __delete_conf(chat_id: int) -> None:
    chat: Optional[Conf] = Conf.get_or_none(tg_id=chat_id)
    if chat is not None:
        chat.delete_instance()


@check_if_admin
async def delete_conf_data(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await process_message(update, context)

    if is_edit_or_forward(update):
        return

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Dropping all data for your conf, please wait.."
    )
    __delete_conf(update.effective_message.chat_id)

    logger.info(f"User {update.effective_user.full_name} dropped conf data for {update.effective_chat.full_name} conf")
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Successfully dropped all the conf data"
    )


@check_if_admin
async def update_conf_database(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await process_message(update, context)

    if is_edit_or_forward(update):
        return

    this_conf: Conf = Conf.get(tg_id=update.effective_chat.id)
    this_conf_members: List[ConfMember] = list(ConfMember.select().where(
        (ConfMember.conf == this_conf) &
        (ConfMember.is_active == True)
        )
    )

    for conf_member in this_conf_members:
        try:
            chat_member = await context.bot.get_chat_member(
                chat_id=this_conf.tg_id,
                user_id=conf_member.tg_id
            )
        except telegram.error.BadRequest as e:
            if e.message == "User not found":
                conf_member.is_active = False
                conf_member.save()

                logger.warning(
                    f"Couldn't find member {conf_member} in conf {update.effective_chat.id}, "
                    "marking them as inactive"
                )
                await context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    text=f"Marked {conf_member.name} as inactive"
                )
                continue
            logger.exception(f"Got an unexpected error when trying to update {this_conf.tg_id}", exc_info=e)
            continue

        if conf_member.name != chat_member.user.full_name:
            conf_member.name = chat_member.user.full_name
            conf_member.save()

    await update.effective_message.reply_text("Successfully updated the database for this conf")
