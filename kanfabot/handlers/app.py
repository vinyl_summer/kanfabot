import inspect
import logging
from typing import List

import peewee
import telegram.error
from telegram import Update, Bot
from telegram.ext import ContextTypes

from kanfabot import version_manager
from kanfabot.models.models import Conf
from kanfabot.utils.utils import check_if_admin, process_message


logger = logging.getLogger(__name__)


async def send_startup_message(context: ContextTypes.DEFAULT_TYPE) -> None:
    bot: Bot = context.job.data
    startup_message: str = f"Running version {version_manager.get_current_version()}"
    all_confs: List[Conf] = list(Conf.select())
    for conf in all_confs:
        try:
            await bot.send_message(chat_id=conf.tg_id, text=startup_message)
        except telegram.error.ChatMigrated as e:
            logger.info(f"Conf {conf.tg_id} migrated from group to supergroup, setting tg id to {e.new_chat_id}")

            try:
                conf.tg_id = e.new_chat_id
                conf.save()
            except peewee.IntegrityError as e:
                logger.exception(f"Couldn't set new chat id for {conf.tg_id}, aborting", exc_info=e)
                continue

            await bot.send_message(chat_id=conf.tg_id, text=startup_message)
        except telegram.error.BadRequest as e:
            logger.exception(f"Couldn't send startup message in conf {conf.tg_id}", exc_info=e)
            continue
        except Exception as e:
            logger.exception(f"Unexpected error for conf {conf.tg_id}", exc_info=e)
            continue


@check_if_admin
async def show_version(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")
    await process_message(update, context)
    await update.message.reply_text(text=f"Current version is: {version_manager.get_current_version()}")
