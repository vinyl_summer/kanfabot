import inspect
import logging
import os

import requests
from telegram import Update
from telegram.ext import ContextTypes

from kanfabot.utils.utils import check_if_admin, process_message, is_edit_or_forward

logger = logging.getLogger(__name__)


# Enables the KFC sale coupon notifications in the group (admin only)
@check_if_admin
async def enable_5050(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await process_message(update, context)

    if is_edit_or_forward(update):
        return

    this_chat_id = update.effective_chat.id
    try:
        assert "COUPON_API" in os.environ
    except AssertionError:
        logger.error("Couldn't find COUPON_API env variable while trying to enable 5050 alerts")
        await context.bot.send_message(chat_id=this_chat_id, text="check env vars")
        return

    if len(context.job_queue.get_jobs_by_name('5050 alert')) == 0:
        context.job_queue.run_repeating(
            callback=send_5050,
            first=1,
            interval=60 * 60 * 8,
            chat_id=this_chat_id,
            name='5050 alert',
            data=this_chat_id
        )

        logger.info(f"5050 alerts enabled for {update.effective_chat.full_name} chat")
        await context.bot.send_message(chat_id=this_chat_id, text="5050 alerts enabled")


# Disables the KFC sale coupon notifications in the group (admin only)
@check_if_admin
async def disable_5050(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await process_message(update, context)

    if is_edit_or_forward(update):
        return

    for job in context.job_queue.get_jobs_by_name('5050 alert'):
        job.schedule_removal()

    logger.info(f"5050 alerts disabled for {update.effective_chat.full_name} chat")
    await context.bot.send_message(chat_id=update.effective_message.chat_id, text="5050 alerts disabled")


# TODO: fix the API (optional)
# Sends notifications to the group about new KFC sale coupon
async def send_5050(context: ContextTypes.DEFAULT_TYPE) -> None:
    r = requests.get(os.environ["COUPON_API"], timeout=30)

    try:
        r.raise_for_status()
    except requests.exceptions.HTTPError as e:
        logger.error(e)
        return

    coupon = r.json()
    meal = coupon['meal']
    # price = coupon['price']
    chat_id = context.job.chat_id

    if meal is not None:
        await context.bot.send_message(
            chat_id=chat_id,
            text=f"Сегодня среда мои чюваки. Новый 5050 купон: {meal}")
