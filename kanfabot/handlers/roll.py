import inspect
import logging
from datetime import datetime, timedelta
from typing import List, Optional, Literal

import peewee
import telegram

from telegram import Update, Message
from telegram.ext import ContextTypes, CallbackContext

from kanfabot.handlers.conf import _apply_chance_reactions
from kanfabot.handlers.member import get_or_create_conf_member
from kanfabot.models.models import ConfMember, Roll, Conf
from kanfabot.utils import utils

logger = logging.getLogger(__name__)

MAX_ROLLS: int = 1
TIME_PERIOD_MINUTES: int = 60


async def roll(update: Update, context: CallbackContext) -> None:
    logger.debug(f"Handler {inspect.currentframe().f_code.co_name} was invoked by {update}")

    await utils.process_message(update, context)

    if utils.is_edit_or_forward(update):
        return

    random_chance: int = utils.get_random_chance()
    user_message: str = ' '.join(update.effective_message.text.split()[2:])
    member: ConfMember = get_or_create_conf_member(update.effective_user, update.effective_chat)

    if update.effective_message.is_from_offline:
        _ban_user_rolling(member, 24)
        await update.effective_message.reply_text("не откладывай на потом то, что можно сделать сейчас")
        return
    can_roll = await _roll_rate_limit(update, context, member)
    if not can_roll:
        return

    scored: bool = await _roll(update, random_chance)

    await _apply_roll_reactions(
        target_message=update.effective_message,
        user_message=user_message,
        random_chance=random_chance,
        scored=scored,
    )

    response = f"Шанс того, что {user_message} равен {random_chance}%"
    if scored:
        rolls_since_last_score: int
        is_first_score: bool
        rolls_since_last_score, is_first_score = _get_rolls_since_last_score(member)

        if is_first_score:
            response += f"\nCongrats on your first score in {rolls_since_last_score} rolls"
        else:
            response += f"\nCongrats! It has been {rolls_since_last_score} rolls since you last scored"

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=response
    )
    if scored:
        await show_727_leaderboard(update, context)


def _get_ban_duration(member: ConfMember) -> Optional[timedelta]:
    now: datetime = datetime.utcnow()

    if not member.unban_datetime:
        return None

    if now >= member.unban_datetime:
        member.unban_datetime = None
        member.save()
        return None
    else:
        time_until_unban: timedelta = member.unban_datetime - now
        return time_until_unban


def _get_cooldown(member: ConfMember) -> Optional[timedelta]:
    now: datetime = datetime.utcnow()

    some_minutes_ago: datetime = now - timedelta(minutes=TIME_PERIOD_MINUTES)

    recent_rolls: peewee.ModelSelect = (Roll
                                        .select()
                                        .where((Roll.owner == member) & (Roll.timestamp.between(some_minutes_ago, now)))
                                        .order_by(Roll.timestamp.desc())
                                        )
    recent_rolls_list: List[Roll] = list(recent_rolls)

    if len(recent_rolls_list) >= MAX_ROLLS:
        last_roll: Roll = recent_rolls_list[0]
        time_since_last_roll: timedelta = datetime.utcnow() - last_roll.timestamp

        # Copied from timedelta on docs.python.org
        # timedelta Representation: (days, seconds, microseconds). Why? Because I felt like it.
        cooldown = timedelta(minutes=TIME_PERIOD_MINUTES) - time_since_last_roll
        return cooldown

    return None


async def _roll_rate_limit(update: Update, context: ContextTypes.DEFAULT_TYPE, member: ConfMember) -> bool:
    ban_duration: timedelta = _get_ban_duration(member)
    if ban_duration:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Вас заьанили, осталось ждать {utils.format_timedelta(ban_duration)}")

        logger.info(f"User {update.effective_user.full_name} tried to roll when banned,\n"
                    f"{utils.format_timedelta(ban_duration)} left until unban.")
        return False

    cooldown: timedelta = _get_cooldown(member)
    if cooldown:
        response_text = f"Следующий ролл будет доступен через {utils.format_timedelta(cooldown)}"

        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=response_text)

        logger.info(f"User {update.effective_user.full_name} tried to roll when on cooldown,\n"
                    f"{utils.format_timedelta(cooldown)} left until next roll "
                    f"and the cooldown is {TIME_PERIOD_MINUTES} minutes.")
        return False
    return True


async def _roll(update: Update, random_chance: int) -> bool:
    member: ConfMember = get_or_create_conf_member(update.effective_user, update.effective_chat)
    user_message: str = ' '.join(update.effective_message.text.split()[2:])
    this_conf = Conf.get(tg_id=update.effective_chat.id)

    new_roll: Roll = Roll.create(
        owner=member,
        origin_conf=this_conf,
        timestamp=datetime.utcnow(),
        type=user_message,
        result=random_chance
    )
    new_roll.save()

    scored: bool = False
    win_condition = (user_message == '7' and random_chance == 27) or (user_message == '72' and random_chance == 7)
    if win_condition:
        logger.info(f"User {update.effective_user.full_name} rolled {user_message} {random_chance} and won!")
        member.score_727 += 1
        scored = True

    member.total_rolls += 1
    member.save()

    return scored


# takes a ConfMember obj, returns the number of rolls since the member last scored and if it's his first score
def _get_rolls_since_last_score(member: ConfMember) -> (int, bool):
    score_condition: bool = (((Roll.type == '7') & (Roll.result == '27')) |
                             ((Roll.type == '72') & (Roll.result == '7')))
    member_scores: List[Roll] = list(Roll.
                                     select().
                                     where(score_condition & (Roll.owner == member)).
                                     order_by(Roll.timestamp.desc())
                                     )

    if len(member_scores) <= 1:
        all_member_rolls: int = (Roll.
                                 select().
                                 where(Roll.owner == member).
                                 count()
                                 )
        return all_member_rolls, True

    last_score: Roll = member_scores[1]
    rolls_since_last_score: int = (Roll.
                                   select().
                                   where((Roll.timestamp > last_score.timestamp) &
                                         (Roll.owner == member)).
                                   count()
                                   )

    # subtract 1 since we don't want to count the winning roll
    return rolls_since_last_score - 1, False


async def _apply_roll_reactions(
        target_message: Message,
        user_message: Literal['72', '7'],
        random_chance: int,
        scored: bool
) -> None:
    await _apply_chance_reactions(target_message, user_message, random_chance)

    # off by one
    if (user_message == '7' and random_chance in {26, 28}) or (user_message == '72' and random_chance in {6, 8}):
        await target_message.set_reaction(telegram.constants.ReactionEmoji.PILE_OF_POO)

    # got a chance that would score for the other roll
    if (user_message == '72' and random_chance == 27) or (user_message == '7' and random_chance == 7):
        await target_message.set_reaction(telegram.constants.ReactionEmoji.ROLLING_ON_THE_FLOOR_LAUGHING)

    if scored:
        await target_message.set_reaction(telegram.constants.ReactionEmoji.PARTY_POPPER)


def _calculate_winrate(member: ConfMember) -> float:
    if member.total_rolls == 0:
        return 0
    return round((member.score_727 / member.total_rolls) * 100, 2)


def _calculate_727_ratio(rolls: List[Roll]) -> float:
    if len(rolls) == 0:
        return 0
    return round((sum(r.type == '7' for r in rolls) / len(rolls)) * 100, 1)


async def show_727_leaderboard(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await utils.process_message(update, context)

    if utils.is_edit_or_forward(update):
        return

    this_conf: Optional[Conf] = Conf.get_or_none(tg_id=update.effective_chat.id)
    if this_conf is None:
        return

    query: peewee.ModelSelect = (Roll.
                                 select().
                                 where(Roll.origin_conf == this_conf)
                                 )
    rolls: List[Roll] = list(query)

    if len(rolls) == 0:
        ratio_of_7, ratio_of_72 = 0, 0
    else:
        ratio_of_7 = _calculate_727_ratio(rolls)
        ratio_of_72 = round(100 - ratio_of_7, 1)

    wins_7: int
    wins_72: int

    wins_7 = (Roll.
              select().
              where(
                (Roll.origin_conf == this_conf) &
                (Roll.type == "7") &
                (Roll.result == 27)).
              count()
              )

    wins_72 = (Roll.
               select().
               where(
                (Roll.origin_conf == this_conf) &
                (Roll.type == "72") &
                (Roll.result == 7)).
               count()
               )
    wins = wins_7 + wins_72
    if wins == 0:
        winrate_7, winrate_72 = 0, 0
    else:
        winrate_7: float = round((wins_7 / wins) * 100, 1)
        winrate_72: float = round(100 - winrate_7, 1)

    query: peewee.ModelSelect
    query = (ConfMember.
             select().
             where(
                (ConfMember.conf == this_conf) &
                (ConfMember.is_active == True) &
                (ConfMember.score_727 > 0)).
             order_by(
                 ConfMember.score_727.desc()
             )
             )
    conf_rolling_members: List[ConfMember] = list(query)
    leaderboard = "\n".join(
        [
            f"{i + 1}) {member.name}: {member.score_727},"
            f" wr: {_calculate_winrate(member)}%"
            for i, member in enumerate(conf_rolling_members)
        ]
    )

    if leaderboard == '':
        response = "Good luck getting a score"
    else:
        response = ("727 leaderboard:\n" +
                    leaderboard +
                    f"\n\n"
                    f"Total:\n7 - {ratio_of_7}%, 72 - {ratio_of_72}%"
                    f"\nWr:\n7 - {winrate_7}%, 72 - {winrate_72}%"
                    )

    await update.message.reply_text(text=response)


# raises OverflowError if duration_hours is too high
def _ban_user_rolling(member: ConfMember, duration_hours: float) -> None:
    duration_hours_timedelta: timedelta = timedelta(hours=duration_hours)

    if member.unban_datetime:
        member.unban_datetime += duration_hours_timedelta
    else:
        member.unban_datetime = datetime.utcnow() + duration_hours_timedelta
    member.save()


@utils.check_if_admin
async def ban_user_rolling(update: Update, context: CallbackContext) -> None:
    await utils.process_message(update, context)

    original_message: Message | None = update.effective_message.reply_to_message
    if not original_message:
        await update.effective_message.reply_text(text="Usage: reply with /ban <time period hours>")
        logger.info(f"Invalid input in restrict_user from {update.effective_user.full_name}")
        return

    message_text_list: List[str] = update.effective_message.text.split()
    if len(message_text_list) < 2:
        await update.effective_message.reply_text(text="Usage: reply with /ban <time period hours>")
        logger.info(f"Invalid input in restrict_user from {update.effective_user.full_name}")
        return

    time_period_hours_str: str = message_text_list[-1]
    target_id: int = original_message.from_user.id

    try:
        time_period_hours = float(time_period_hours_str)
    except ValueError:
        await update.effective_message.reply_text(text="Usage: reply with /ban <time period hours>")
        logger.info(f"Invalid input in restrict_user from {update.effective_user.full_name}")
        return

    this_conf: Conf = Conf.get(Conf.tg_id == update.effective_chat.id)
    conf_member_to_ban: Optional[ConfMember] = ConfMember.get_or_none(
        ConfMember.conf == this_conf,
        ConfMember.tg_id == target_id
    )

    if not conf_member_to_ban:
        await update.effective_message.reply_text(text="Service error, couldn't find the member in the database")
        logger.info(f"Couldn't find {target_id} from {this_conf.tg_id} in the database")
        return

    try:
        _ban_user_rolling(conf_member_to_ban, time_period_hours)
    except OverflowError as e:
        logger.error(f"User: {update.effective_user.full_name} "
                     f"from {update.effective_chat.id} "
                     f"tried to ban {conf_member_to_ban} "
                     f"for {time_period_hours} hours "
                     f"causing {e}")
        await update.effective_message.reply_text("чювак не доживёт")

    await update.effective_message.reply_text(f"{conf_member_to_ban.name} получает ьан на {time_period_hours} часов")
    logger.info(f"User: {update.effective_user.full_name} "
                f"from {update.effective_chat.id} "
                f"restricted rolling for "
                f"{conf_member_to_ban.name} {target_id}, "
                f"{time_period_hours} hours ban")


@utils.check_if_admin
async def unban_user_rolling(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await utils.process_message(update, context)

    original_message: Message | None = update.effective_message.reply_to_message
    if not original_message:
        await update.effective_message.reply_text(text="Usage: reply with /unban")
        logger.info(f"Invalid input in unban_user from {update.effective_user.full_name}")
        return

    target_id: int = original_message.from_user.id

    this_conf: Conf = Conf.get(Conf.tg_id == update.effective_chat.id)
    conf_member_to_unban: Optional[ConfMember] = ConfMember.get_or_none(
        ConfMember.conf == this_conf,
        ConfMember.tg_id == target_id
    )

    if not conf_member_to_unban:
        await update.effective_message.reply_text(text="Service error, couldn't find the member in the database")
        logger.info(f"Couldn't find {target_id} from {this_conf.tg_id} in the database")
        return

    if conf_member_to_unban.unban_datetime:
        conf_member_to_unban.unban_datetime = None
        conf_member_to_unban.save()

        await update.effective_message.reply_text(f"{conf_member_to_unban.name} был разьанен")
        logger.info(f"User: {update.effective_user.full_name} {update.effective_user.id} "
                    f"from {update.effective_chat.id} "
                    f"unbanned "
                    f"{conf_member_to_unban.name}")
    else:
        await update.effective_message.reply_text(f"{conf_member_to_unban.name} и так не заьанен")
        logger.info(f"User: {update.effective_user.full_name} {update.effective_user.id} "
                    f"from {update.effective_chat.id} "
                    f"tried to unban "
                    f"{conf_member_to_unban.name} "
                    f"but he wasn't banned")
