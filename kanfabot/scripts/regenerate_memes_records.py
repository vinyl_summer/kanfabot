import os
import sys
import uuid
from argparse import ArgumentParser

from peewee import SqliteDatabase

from kanfabot.models.models import Meme, Conf
from kanfabot.utils.utils import get_mime_type, get_db_type_from_mime

# This script assumes that all your memes inside the meme folder belong to the same conf!

parser = ArgumentParser()

parser.add_argument(
    "-m",
    "--memes",
    dest="memes_path",
    help="Path to memes folder",
    required=True
)
parser.add_argument(
    "-d",
    "--database",
    dest="db_path",
    help="Path to database file",
    required=True
)
parser.add_argument("-c",
                    "--conf",
                    dest="conf_tg_id",
                    help="Conf id to associate the memes with, optional",
                    required=False
                    )

args = parser.parse_args()

if not os.path.exists(args.memes_path):
    print("Error! Provided memes folder does not exist")
    sys.exit(1)

if not os.path.exists(args.db_path):
    print("Error! Provided database file does not exist")
    sys.exit(1)


db = SqliteDatabase(args.db_path)

conf = Conf.get_or_none(id=args.conf_tg_id)
if conf is None:
    user_input = input("could not find conf, type Confirm to proceed with conf == None: ")
    if user_input != "Confirm":
        sys.exit(0)

for meme in os.listdir(args.memes_path):
    meme_path = os.path.join(args.memes_path, meme)
    if os.path.isfile(meme_path):
        # delete meme binding if it exists
        meme = Meme.get_or_none(uuid=meme)
        if meme is not None:
            meme.delete_instance()

        # create new binding and rename the file accordingly
        new_uuid = str(uuid.uuid4())
        mime_type = get_mime_type(meme_path)
        db_type = get_db_type_from_mime(mime_type)
        try:
            os.rename(meme_path, os.path.join(args.memes_path, new_uuid))
        except PermissionError:
            print(f"Error! Permission denied on renaming meme {os.path.abspath(meme)} to new uuid")
            sys.exit(1)
        Meme.create(uuid=new_uuid, type=db_type, mime_type=mime_type, belongs_to=conf)
