-- removes the duplicate users! the one with the most total_rolls remains
WITH duplicates_table AS(
    SELECT
        conf_members.tg_id,
        conf_members.conf_id,
        conf_members.total_rolls
    FROM
        conf_members
    GROUP BY
        tg_id,
        conf_id
    HAVING
        COUNT(*) > 1
)
DELETE FROM
    conf_members
WHERE
    (tg_id, conf_id) IN (
        SELECT
            duplicates_table.tg_id, duplicates_table.conf_id
        FROM
            duplicates_table
    )
    AND
    total_rolls < (
        SELECT
            duplicates_table.total_rolls
        FROM
            duplicates_table
        WHERE
            conf_members.tg_id = duplicates_table.tg_id
            AND
            conf_members.conf_id = duplicates_table.conf_id
    );

-- at the moment of writing the only important user data is their rolls data
-- if there's 0 rolls total, might as well delete the user
DELETE FROM
   conf_members
WHERE
    total_rolls = 0;
