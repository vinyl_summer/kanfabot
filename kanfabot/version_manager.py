import logging
import subprocess


logger = logging.getLogger(__name__)


def get_current_version() -> str:
    logger.info("Retrieving current app version..")
    try:
        with open("version") as version_file:
            current_version: str = version_file.readline()
        logger.info(f"Version from version file: {current_version}")
    except FileNotFoundError:
        current_version: str = (subprocess.
                                check_output(["git",
                                              "describe",
                                              "--dirty=-modified",
                                              "--broken",
                                              "--tags",
                                              ]).
                                decode("utf-8").
                                strip()
                                )
        logger.info(f"Version from git describe: {current_version}")

    return current_version
