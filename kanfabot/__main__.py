import logging
import os

from kanfabot.app.app import start_bot

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
    force=True
)
logging.getLogger("httpx").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)


assert "API_TOKEN" in os.environ
logger.info("Made sure that API_TOKEN environment variable is set")

bot_token = os.environ["API_TOKEN"]
start_bot(bot_token)
