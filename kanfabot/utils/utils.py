import random

from datetime import timedelta
from typing import Callable, Coroutine

import magic

from telegram import Update, Chat, User, Bot, ChatMember
from telegram.constants import ChatType, ChatMemberStatus
from telegram.ext import ContextTypes

from kanfabot.handlers.member import get_or_create_conf_member
from kanfabot.models.models import Meme


def is_edit_or_forward(update: Update) -> bool:
    if update.edited_message is not None or update.effective_message.forward_origin is not None:
        return True
    return False


def check_if_admin(func: Callable[[Update, ContextTypes.DEFAULT_TYPE], Coroutine]) \
        -> Callable[[Update, ContextTypes.DEFAULT_TYPE], Coroutine]:
    async def wrapper(update: Update, context: ContextTypes.DEFAULT_TYPE) -> Coroutine:
        chat: Chat = update.effective_chat
        user: User = update.effective_user
        bot: Bot = context.bot

        member: ChatMember = await bot.get_chat_member(chat_id=chat.id, user_id=user.id)

        if (chat.type == ChatType.PRIVATE or
                member.status in [ChatMemberStatus.ADMINISTRATOR, ChatMemberStatus.OWNER]):
            return await func(update, context)

    return wrapper


def get_mime_type(meme_path: str) -> str:
    mime = magic.Magic(mime=True)
    return mime.from_file(meme_path)


def get_db_type_from_mime(mime: str) -> str:
    meme_types = {
        "video/mp4": Meme.VIDEO,

        "audio/mp3": Meme.AUDIO,
        "audio/m4a": Meme.AUDIO,
        "audio/x-m4a": Meme.AUDIO,

        "audio/ogg": Meme.VOICE,

        "image/jpeg": Meme.PHOTO,
        "image/jpg": Meme.PHOTO,
    }
    return meme_types.get(mime) or Meme.DOCUMENT


def format_timedelta(duration: timedelta) -> str:
    if duration >= timedelta(days=365):
        years = duration.days // 365
        new_duration = duration - timedelta(days=years * 365)
        months = new_duration.days // 30
        return (f"{years} year{'s' if years > 1 else ''}, "
                f"{months} day{'s' if months > 1 else ''}")
    if duration >= timedelta(days=30):
        months = duration.days // 30
        new_duration = duration - timedelta(days=months * 30)
        days = new_duration.days
        return (f"{months} month{'s' if months > 1 else ''}, "
                f"{days} day{'s' if days > 1 else ''}")
    if duration >= timedelta(days=1):
        new_duration = duration - timedelta(days=duration.days)
        hours = new_duration.seconds // 3600
        return (f"{duration.days} day{'s' if duration.days > 1 else ''}, "
                f"{hours} hour{'s' if hours > 1 else ''}")
    if duration >= timedelta(hours=1):
        hours = duration.seconds // 3600
        new_duration = duration - timedelta(hours=hours)
        minutes = new_duration.seconds // 60
        return (f"{hours} hour{'s' if hours > 1 else ''}, "
                f"{minutes} minute{'s' if minutes > 1 else ''}")
    if duration >= timedelta(minutes=1):
        minutes = duration.seconds // 60
        new_duration = duration - timedelta(minutes=minutes)
        seconds = new_duration.seconds
        return (f"{minutes} minute{'s' if minutes > 1 else ''}, "
                f"{seconds} second{'s' if seconds > 1 else ''}")
    if duration >= timedelta(seconds=1):
        return f"{duration.seconds} second{'s' if duration.seconds > 1 else ''}"


def get_random_chance() -> int:
    return random.randint(0, 101)


# Message processing function that applies after all the other callbacks, intended to process the message in some ways
async def process_message(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    # Only add users from groups to the database
    groups = [ChatType.GROUP, ChatType.SUPERGROUP]
    if update.effective_chat.type in groups:
        # if someone added a new member, both will be added to the database
        if update.effective_message.new_chat_members:
            get_or_create_conf_member(update.effective_message.new_chat_members[0], update.effective_chat)
        get_or_create_conf_member(update.effective_message.from_user, update.effective_chat)
