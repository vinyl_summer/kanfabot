import logging
import os
from enum import Enum

from peewee import (SqliteDatabase, Model,
                    CharField, IntegerField,
                    BooleanField, TextField,
                    ForeignKeyField, UUIDField,
                    Check, DateTimeField, SQL, )
from peewee_migrate import Router

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logging.getLogger("httpx").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

assert "DB_URL" in os.environ
logger.info("Made sure that DB_URL environment variable is set")
db = SqliteDatabase(os.environ["DB_URL"])
logger.info(f"Initialized db at {os.path.abspath(os.environ['DB_URL'])}")


class ActionConstant(Enum):
    NO_ACTION = 'NO ACTION'
    RESTRICT = 'RESTRICT'
    SET_NULL = 'SET NULL'
    SET_DEFAULT = 'SET DEFAULT'
    CASCADE = 'CASCADE'

    def __str__(self):
        return self.value


class BaseModel(Model):
    class Meta:
        database = db
        legacy_table_names = False


class Conf(BaseModel):
    tg_id = IntegerField(unique=True)

    type = TextField(
        choices=[('group', 'standard group type'),
                 ('supergroup', 'super group'),
                 ],
        default='group'
    )

    GROUP = 'group'
    SUPERGROUP = 'supergroup'

    class Meta:
        table_name = 'confs'


class ConfMember(BaseModel):
    tg_id = IntegerField(unique=False)

    name = CharField(null=True)

    conf = ForeignKeyField(
        Conf,
        backref='members',
        on_delete=ActionConstant.CASCADE
    )

    to_notify_5050 = BooleanField(default=False)

    is_active = BooleanField(default=True)

    # none value indicates that the user isn't banned
    unban_datetime = DateTimeField(null=True, default=None)

    score_727 = IntegerField(default=0)
    total_rolls = IntegerField(default=0)

    class Meta:
        table_name = 'conf_members'
        constraints = [SQL('UNIQUE (tg_id, conf_id)')]


class Meme(BaseModel):
    uuid = UUIDField(unique=True)

    title = TextField(null=True)

    type = CharField(
        choices=[("document", "general file"),
                 ("photo", "photo"),

                 ("audio", "song/audio file, .mp3/.m4a"),
                 ("voice", "ogg audio file, <= 1 MB"),

                 ("video", "mp4 video"),
                 ],
        null=True)

    mime_type = CharField(null=True)

    description = TextField(null=True)

    added_by = ForeignKeyField(
        ConfMember,
        backref='created_memes',
        on_delete=ActionConstant.SET_NULL,
        null=True
    )

    belongs_to = ForeignKeyField(
        Conf,
        backref='added_memes',
        on_delete=ActionConstant.SET_NULL,
        null=True
    )

    DOCUMENT = "document"
    PHOTO = "photo"
    AUDIO = "audio"
    VOICE = "voice"
    VIDEO = "video"

    class Meta:
        table_name = 'memes'


class Roll(BaseModel):
    owner = ForeignKeyField(
        ConfMember,
        backref='rolls',
        on_delete=ActionConstant.CASCADE
    )

    origin_conf = ForeignKeyField(
        Conf,
        backref='rolls',
        on_delete=ActionConstant.CASCADE,
        null=True
    )

    type = CharField(
        choices=[("7", "7"),
                 ("72", "72")
                 ],
        null=True
    )

    result = IntegerField(
        constraints=[Check('result >= 0'), Check('result <= 101')]
    )

    timestamp = DateTimeField(
        constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')]
    )

    class Meta:
        table_name = 'rolls'


def init_db() -> None:
    with db:
        db.pragma('foreign_keys', 1, permanent=True)

    router = Router(SqliteDatabase(os.environ.get("DB_URL")))
    router.run()

    logger.info("Applied all migrations")

    # can be useful if there's SOMEHOW no migrations yet
    db.create_tables([ConfMember, Conf, Meme, Roll])
