"""Peewee migrations -- 008_add tg_id+conf_id unique constraint on conf_member.py.

Some examples (model - class or model name)::

    > Model = migrator.orm['table_name']            # Return model in current state by name
    > Model = migrator.ModelClass                   # Return model in current state by name

    > migrator.sql(sql)                             # Run custom SQL
    > migrator.run(func, *args, **kwargs)           # Run python function with the given args
    > migrator.create_model(Model)                  # Create a model (could be used as decorator)
    > migrator.remove_model(model, cascade=True)    # Remove a model
    > migrator.add_fields(model, **fields)          # Add fields to a model
    > migrator.change_fields(model, **fields)       # Change fields
    > migrator.remove_fields(model, *field_names, cascade=True)
    > migrator.rename_field(model, old_field_name, new_field_name)
    > migrator.rename_table(model, new_table_name)
    > migrator.add_index(model, *col_names, unique=False)
    > migrator.add_not_null(model, *field_names)
    > migrator.add_default(model, field_name, default)
    > migrator.add_constraint(model, name, sql)
    > migrator.drop_index(model, *col_names)
    > migrator.drop_not_null(model, *field_names)
    > migrator.drop_constraints(model, *constraints)

"""

from contextlib import suppress

import peewee as pw
from peewee_migrate import Migrator

with suppress(ImportError):
    import playhouse.postgres_ext as pw_pext


def migrate(migrator: Migrator, database: pw.Database, *, fake=False):
    """Write your migrations here."""

    migration: str = \
        '''
        CREATE TABLE "conf_members_temp" (
            "id" INTEGER NOT NULL PRIMARY KEY,
            "name" VARCHAR(255),
            "tg_id" INTEGER NOT NULL,
            "conf_id" INTEGER NOT NULL,
            "to_notify_5050" INTEGER NOT NULL,
            "score_727" INTEGER NOT NULL,
            "total_rolls" INTEGER NOT NULL,
            "is_active" INTEGER NOT NULL,
            "unban_datetime" DATETIME,
            FOREIGN KEY ("conf_id") REFERENCES "confs" ("id") ON DELETE CASCADE,
            CONSTRAINT unique_tg_id_in_conf UNIQUE (tg_id, conf_id) ON CONFLICT ABORT
        );
    
        INSERT INTO
            conf_members_temp
        SELECT * FROM conf_members;
    
        DROP TABLE conf_members;
    
        ALTER TABLE
            conf_members_temp
        RENAME TO
            conf_members;
    '''

    for statement in migration.split(";"):
        if statement:
            migrator.sql(statement + ";")


def rollback(migrator: Migrator, database: pw.Database, *, fake=False):
    """Write your rollback migrations here."""

    migration: str = \
        '''
        CREATE TABLE "conf_members_temp" (
            "id" INTEGER NOT NULL PRIMARY KEY,
            "name" VARCHAR(255),
            "tg_id" INTEGER NOT NULL,
            "conf_id" INTEGER NOT NULL,
            "to_notify_5050" INTEGER NOT NULL,
            "score_727" INTEGER NOT NULL,
            "total_rolls" INTEGER NOT NULL,
            "is_active" INTEGER NOT NULL,
            "unban_datetime" DATETIME,
            FOREIGN KEY ("conf_id") REFERENCES "confs" ("id") ON DELETE CASCADE
        );

        INSERT INTO
            conf_members_temp
        SELECT * FROM conf_members;

        DROP TABLE conf_members;

        ALTER TABLE
            conf_members_temp
        RENAME TO
            conf_members;
    '''

    for statement in migration.split(";"):
        if statement:
            migrator.sql(statement + ";")
