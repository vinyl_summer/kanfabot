"""Peewee migrations -- 001_initial migration.py.

Some examples (model - class or model name)::

    > Model = migrator.orm['table_name']            # Return model in current state by name
    > Model = migrator.ModelClass                   # Return model in current state by name

    > migrator.sql(sql)                             # Run custom SQL
    > migrator.run(func, *args, **kwargs)           # Run python function with the given args
    > migrator.create_model(Model)                  # Create a model (could be used as decorator)
    > migrator.remove_model(model, cascade=True)    # Remove a model
    > migrator.add_fields(model, **fields)          # Add fields to a model
    > migrator.change_fields(model, **fields)       # Change fields
    > migrator.remove_fields(model, *field_names, cascade=True)
    > migrator.rename_field(model, old_field_name, new_field_name)
    > migrator.rename_table(model, new_table_name)
    > migrator.add_index(model, *col_names, unique=False)
    > migrator.add_not_null(model, *field_names)
    > migrator.add_default(model, field_name, default)
    > migrator.add_constraint(model, name, sql)
    > migrator.drop_index(model, *col_names)
    > migrator.drop_not_null(model, *field_names)
    > migrator.drop_constraints(model, *constraints)

"""

from contextlib import suppress

import peewee as pw
from peewee_migrate import Migrator


with suppress(ImportError):
    import playhouse.postgres_ext as pw_pext


def migrate(migrator: Migrator, database: pw.Database, *, fake=False):
    """Write your migrations here."""
    
    @migrator.create_model
    class BaseModel(pw.Model):
        id = pw.AutoField()

        class Meta:
            table_name = "base_model"

    @migrator.create_model
    class Conf(pw.Model):
        id = pw.IntegerField(primary_key=True)
        type = pw.TextField(default='group')

        class Meta:
            table_name = "confs"

    @migrator.create_model
    class ConfMember(pw.Model):
        id = pw.AutoField()
        name = pw.CharField(max_length=255, null=True)
        tg_id = pw.IntegerField()
        conf = pw.ForeignKeyField(column_name='conf_id', field='id', model=migrator.orm['confs'], on_delete='CASCADE')
        to_notify_5050 = pw.BooleanField(default=False)
        score_727 = pw.IntegerField(default=0)
        total_rolls = pw.IntegerField(default=0)

        class Meta:
            table_name = "conf_members"

    @migrator.create_model
    class Meme(pw.Model):
        id = pw.AutoField()
        uuid = pw.UUIDField(unique=True)
        title = pw.TextField(null=True)
        type = pw.CharField(max_length=255)
        mime_type = pw.CharField(max_length=255, null=True)
        description = pw.TextField(null=True)
        added_by = pw.ForeignKeyField(column_name='added_by_id', field='id', model=migrator.orm['conf_members'], null=True, on_delete='SET NULL')
        belongs_to = pw.ForeignKeyField(column_name='belongs_to_id', field='id', model=migrator.orm['confs'], null=True, on_delete='SET NULL')

        class Meta:
            table_name = "memes"

    @migrator.create_model
    class Roll(pw.Model):
        id = pw.AutoField()
        owner = pw.ForeignKeyField(column_name='owner_id', field='id', model=migrator.orm['conf_members'], on_delete='CASCADE')
        result = pw.IntegerField()
        timestamp = pw.DateTimeField()

        class Meta:
            table_name = "rolls"


def rollback(migrator: Migrator, database: pw.Database, *, fake=False):
    """Write your rollback migrations here."""
    
    migrator.remove_model('rolls')

    migrator.remove_model('memes')

    migrator.remove_model('conf_members')

    migrator.remove_model('confs')

    migrator.remove_model('base_model')
