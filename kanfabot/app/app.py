import logging
import re

from telegram import Update
from telegram.ext import ApplicationBuilder, Application, MessageHandler, filters, CommandHandler

from kanfabot.models.models import init_db
from kanfabot.handlers.meme import init_memes_folder

from kanfabot.handlers import conf, roll, app, kfc5050
from kanfabot.handlers import meme
from kanfabot.utils import utils

logger = logging.getLogger(__name__)

WHO_CASES = r'(кто|кого|у кого|кому|по кому|кем|с кем|о ком)'


def start_bot(bot_token: str):
    # Create bot application
    application: Application = ApplicationBuilder().token(bot_token).build()
    logger.info("Successfully built PTB application")

    init_db()
    init_memes_folder()

    # Common bot commands
    command_prefix = r'^(а|соня)'
    application.add_handlers(
        [
            MessageHandler(
                filters.CaptionRegex(re.compile(rf'{command_prefix} мем', re.IGNORECASE)) |
                (filters.Regex(re.compile(rf'{command_prefix} мем', re.IGNORECASE))),
                meme.add_meme
            ),

            #            MessageHandler(
            #                filters.Regex(re.compile(rf'{command_prefix} шанс (7|72)$', re.IGNORECASE)),
            #                roll.roll
            #            ),

            MessageHandler(
                filters.Regex(re.compile(rf'{command_prefix} шанс', re.IGNORECASE)),
                conf.send_random_chance
            ),

            MessageHandler(
                filters.Regex(re.compile(rf'{command_prefix} выбери', re.IGNORECASE)),
                conf.send_random_choice
            ),

            MessageHandler(
                filters.Regex(re.compile(rf'{command_prefix} {WHO_CASES}', re.IGNORECASE)),
                conf.send_random_participant_name
            ),

            MessageHandler(
                filters.Regex(re.compile(rf'{command_prefix} кола', re.IGNORECASE)),
                conf.create_timer_job
            ),

            MessageHandler(
                filters.Regex(re.compile(rf'{command_prefix} кидай -?[0-9]+$', re.IGNORECASE)),
                meme.send_meme_by_index
            ),

            MessageHandler(
                filters.Regex(re.compile(rf'{command_prefix} кидай', re.IGNORECASE)),
                meme.send_random_meme
            ),

            CommandHandler(
                '727',
                roll.show_727_leaderboard
            ),

            MessageHandler(
                filters.StatusUpdate.NEW_CHAT_MEMBERS,
                conf.add_conf_member
            ),

            MessageHandler(
                filters.StatusUpdate.LEFT_CHAT_MEMBER,
                conf.remove_conf_member
            ),
        ]
    )

    # Commands that are private chat only or group admin only
    application.add_handlers(
        [
            CommandHandler("enable5050", kfc5050.enable_5050),

            CommandHandler("disable5050", kfc5050.disable_5050),

            CommandHandler("dropDB", conf.delete_conf_data),

            CommandHandler("show", conf.show_members),

            CommandHandler("ban", roll.ban_user_rolling),

            CommandHandler("unban", roll.unban_user_rolling),

            CommandHandler("version", app.show_version),

            CommandHandler("updateDB", conf.update_conf_database)
        ]
    )

    # If a message isn't a bot command, it gets processed in some ways
    # (e.g., add a user to the database if they aren't already in it)
    application.add_handler(
        MessageHandler(
            filters.ALL,
            utils.process_message
        )
    )

    logger.info("Successfully initialized and added all the update handlers")

    # send the startup message to every conf
    application.job_queue.run_once(
        callback=app.send_startup_message,
        when=0,
        data=application.bot
    )

    # Start the application
    application.run_polling(
        drop_pending_updates=True,
        timeout=60,
        allowed_updates=Update.ALL_TYPES
    )
