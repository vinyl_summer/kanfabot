# Kanfa Bot
I made this Telegram Bot for my friends group for fun!

## Commands List
1) А шанс (something) - returns a random chance of something happening
2) А выбери (list of options) - returns one of the specified options, they must be separated by spaces, commas or "или" keyword
3) А кола (minutes) - notifies the user after a set period of time
4) А кидай - sends a random meme from the memes folder
5) А мем - adds a meme to the memes folder (the command should be a caption to the meme or reply to the meme message)

## Management Commands List
1) /enable5050 - enables group notifications about KFC sales coupon 5050 (you must add coupon_api URL for this to work)
2) /disable5050 - disables group notifications about KFC sales coupon 5050 (you must add coupon_api URL for this to work)

## Usage

### Manual
1) Create and activate a new virtual environment
``` shell
python -m venv /path/to/new/virtual/environment
```
``` shell
source <new_venv_path>/bin/activate
```
2) Install the project requirements
```shell
pip install -r requirements.txt
```
3) Set the following environment variables:
```shell
export API_TOKEN=<your TG bot api token, mandatory>
```
```shell
export DB_URL=<path to the database file, mandatory>
```
```shell
export COUPON_API=<coupon_api URL, optional>
```
4) Start the script:
```shell
python -m kanfabot
```

### Docker
After building the image, run it with the same environment variables set:
```shell
docker build -t kanfabot .
```
```shell
docker run -d --env=API_TOKEN=<TG bot api token> --env=DB_URL=<db file path> --env=COUPON_API=<coupon_api URL> kanfabot
```
## Database migrations
[peewee_migrate](https://github.com/klen/peewee_migrate) is used as a migration engine. Don't forget to export DB_URL env variable when using migration commands!
Example:
```shell
 DB_URL=<DB_URL> pw_migrate create --auto --auto-source=models --database=<DB_URL> --directory=migrations <migration name>
```
## Helper scripts
1) regenerate_memes_records.py

In case your database corrupts or you lose access to it, you can regenerate the bindings between memes and their respective database records.

usage: regenerate_memes_records.py [-h] -m MEMES_PATH -d DB_PATH -c CONF_TG_ID

