FROM python:3.12-slim-bullseye
ENV PYTHONBUFFERED=1
RUN mkdir /app
RUN mkdir /app/memes
RUN apt-get update && apt-get install -y libmagic-dev
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
CMD python -m kanfabot
